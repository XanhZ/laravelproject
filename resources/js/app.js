const { vue } = require('laravel-mix');

require('./bootstrap');

window.Vue = require('vue').default;

Vue.component('top-comp', require('./components/HeaderComp.vue').default);
// Vue.component('login-form', require('./components/LoginFormComp.vue').default);
// Vue.component('register-form', require('./components/RegisterFormComp.vue').default);

const app = new Vue({
    el: '#app',
    components: {
        
    }
});
