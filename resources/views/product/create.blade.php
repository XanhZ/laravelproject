<form action="{{ route('product.store') }}" method="POST">
    @csrf
    <div>
        <label for="name">Name: </label>
        <input type="text" name="name">
        @error('name')
            <span role="alert"><strong>{{ $message }}</strong></span>
        @enderror
    </div>
    <div>
        <button type="submit">Tao san pham</button>
    </div>
</form>