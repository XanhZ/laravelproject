<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <title>Document</title>
</head>
<body>
    <div class="main" id="app">
        <div class="main-container">
            <ul>
                <li data-li="form_login" class="active">Đăng nhập</li>
                <li data-li="form_signin">Đăng kí</li>
            </ul>
            <login-form />
        </div>
    </div>
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>