<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ShippingUnitController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\GuestController;
use App\Models\Product;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/login', function () {
    return view('user/login');
});
Route::get('/sigin', function () {
    return view('user/signin');
});
Route::post('/login', [UserController::class, 'login']);
Route::post('/sigin', [UserController::class, 'sigin']);

Route::group(['prefix' => 'admin'], function () {
    Route::resource('/product', ProductController::class);
    Route::resource('/category', CategoryController::class);
    Route::group(['prefix' => 'order'], function () {
        Route::get('/{type}', [OrderController::class, 'index'])->where('type', '[a-z]+');
        Route::get('/edit/{id}', [OrderController::class, 'edit'])->where('id', '[0-9]+');
    });
    Route::resource('/shipping_unit', ShippingUnitController::class);
});

Route::group(['prefix' => 'user'], function () {
    Route::group(['prefix' => 'account'], function () {
        Route::get('user/profile', function () {
            return view('user/profile');
        });
        Route::post('/profile', [UserController::class, 'update_profile']);
        Route::get('/address', function () {
            return view('user/address');
        });
        Route::post('/profile', [UserController::class, 'update_address']);
        Route::get('/password', function () {
            return view('user/password');
        });
        Route::post('/password', [UserController::class, 'update_password']);
    });
    Route::group(['prefix' => 'purchase'], function () {
        Route::get('/{type}', [UserController::class, 'purchase'])->where('type', '[a-z]+');
        Route::get('/view/{id}', [UserController::class, 'view_order'])->where('id', '[0-9]+');
    });
});

Route::group(['prefix' => 'seller'], function () {
    Route::resource('/product', ProductController::class);
    Route::group(['prefix' => 'order'], function() {
        Route::get('/{type}', [OrderController::class, 'index'])->where('type', '[a-z]+');
        Route::get('/edit/{id}', [OrderController::class, 'edit'])->where('id', '[0-9]+');
    });
});






// Route::resource('/product', ProductController::class);

// Route::group(['prefix' => 'admin'], function () {
//     Route::get('/users', function () {
//         return '/admin/user';
//     });

//     Route::group(['prefix' => 'slider'], function () {
//         Route::get('/', [SliderController::class, 'show']);
//         Route::get('edit/{id}', [SliderController::class, 'edit'])->where('id', '[0-9]+');
//         Route::get('delete/{id}', [SliderController::class, 'show'])->where('id', '[0-9]+');
//         Route::get('store-status-{status}/{id}', [SliderController::class, 'store'])->where('id', '[0-9]+');
//     });

//     Route::get('category', function () {
//         return '/admin/category';
//     }); 
// });

// Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
