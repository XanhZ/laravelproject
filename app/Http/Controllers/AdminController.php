<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admin;

class AdminController extends Controller
{
    public function index()
    {
        
    }

    public function handle_category()
    {

    }

    public function handle_product()
    {

    }

    public function handle_order()
    {

    }

    public function handle_shipping_unit()
    {
        
    }
}
