<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->mediumText('description');
            $table->unsignedInteger('category_id', false, true);
            $table->string('brand');
            $table->string('origin');
            $table->unsignedDecimal('price', 15, 5);
            $table->unsignedInteger('quantity');
            $table->unsignedInteger('sold');
            $table->decimal('rate');
            $table->string('first_image');
            $table->string('second_image');
            $table->string('third_image');
            $table->string('fourth_image');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
